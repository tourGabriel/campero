<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    
    public function homeAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');

        $restClient = $this->container->get('circle.restclient');
        $response = $restClient->get($apiUrl.'products?access_token='.$apiToken);
        $content_array = json_decode($response->getContent());
        $parameters["products"] = $content_array;
        //$request->getSession()->remove('cartId'); 
        return $this->render('@App/plantilla/home.html.twig',$parameters);
    }

    public function tiendaAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $restClient = $this->container->get('circle.restclient');
        $apiUrl = $this->container->getParameter('api_url');
        try {
            $response = $restClient->get($apiUrl.'products?access_token='.$apiToken);
            $content_array = json_decode($response->getContent());
            
            $parameters["products"] = $content_array;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          } 
        
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/tienda.html.twig',$parameters);
    }

    public function productoAction(Request $request, $slug)
    {  
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        $response = $restClient->get($apiUrl.'products/'.$slug.'?access_token='.$apiToken);
        $product = json_decode($response->getContent());
        $parameters["product"] = $product;
        
        return $this->render('@App/plantilla/producto.html.twig',$parameters);
    }

    public function blogsAction(Request $request)
    {  
        return $this->render('@App/plantilla/blogs.html.twig');
    }

    public function blogAction(Request $request)
    {  
        return $this->render('@App/plantilla/blog.html.twig');
    }

    public function contactoAction(Request $request)
    {  
        return $this->render('@App/plantilla/contacto.html.twig');
    }

    public function carritoAction(Request $request)
    {  
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $cartId = $request->getSession()->get('cartId');
        if ($cartId != null)
        {
            $response = $restClient->get($apiUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
       
        if(count($cart) == 1){
            $cart = $cart[0];
        }
            $parameters["cart"] = $cart;
        }
        else {
            $parameters["cart"] = null;
        }
        
        
        
        return $this->render('@App/plantilla/carrito.html.twig',$parameters);
    }
    public function errorAction(Request $request,$id, $id1)
    {  
        return $this->render('@App/plantilla/error.html.twig');
    }

    public function pagadoAction(Request $request, $id)
    {  
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $response = $restClient->get($apiUrl.'products/shopping-cart/'.$id.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
        
        $parameters["cart"] = $cart[0];
        $request->getSession()->remove('cartId');
        
        return $this->render('@App/plantilla/pagado.html.twig',$parameters);
    }
    public function rechazoAction(Request $request,$id)
    {  
        return $this->render('@App/plantilla/rechazo.html.twig');
    }
    /*
    public function Action(Request $request)
    {  
        return $this->render('@App/plantilla/.html.twig');
    }

    public function Action(Request $request)
    {  
        return $this->render('@App/plantilla/.html.twig');
    }
    */

   
    public function addToCartAction(Request $request)
    {
        $restClient = $this->container->get('circle.restclient');

        $productObject["product_id"] = $request->get('product_id');
        $productObject["variant_id"] = $request->get('variant_id');
        $productObject["quantity_products"] = $request->get('quantity_products');
        
        if($request->getSession()->get('cartId') == null){
            
            $productObject["cart_id"] = null;
        }
        else {  
            $productObject["cart_id"] = $request->getSession()->get('cartId');
        }
        
        $product = json_encode($productObject);
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $response = $restClient->post($apiUrl.'products/shopping-cart?access_token='.$apiToken,$product);
        $content = json_decode($response->getContent())[0];
       
        $request->getSession()->set('cartId',$content->cart_id);
        
        return $this->redirectToRoute('carrito');
    }
    

    public function deleteFromCartAction(Request $request,$saleId){
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $response = $restClient->delete($apiUrl.'products/shopping-cart/sale/'.strval($saleId).'?access_token='.$apiToken);
        
        $cartId = $request->getSession()->get('cartId');
        if ($cartId != null)
        {
            $response = $restClient->get($apiUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
       
         if($cart[0]->products_sales==[]){
            $request->getSession()->remove('cartId');
         }
        }
        return $this->redirectToRoute('carrito');
    }
    

    public function createCartSessionAction(Request $request, $cartId){
        $request->getSession()->set('cartId',$cartId);
        $response = new Response('Cart session created! ', Response::HTTP_OK);
        return $response;
    }

    public function removeCartSessionAction(Request $request){
        $request->getSession()->remove('cartId');
        $response = new Response('Cart session removed! ', Response::HTTP_OK);
        return $response;
    }

    public function checkoutAction(Request $request){
        $restClient = $this->container->get('circle.restclient');
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $response = $restClient->get($apiUrl.'products/applicate-coupon/'.$cartId.'/'.$code.'?access_token='.$apiToken);
    }
    
    public function resumenAction(Request $request)
    {
       
        
        $apiToken = $this->container->getParameter('api_token');
        $apiUrl = $this->container->getParameter('api_url');
        $restClient = $this->container->get('circle.restclient');
        
        $shippingResponse = $restClient->get($apiUrl.'shipping-methods?access_token='.$apiToken);
    
        $shippingMethods = json_decode($shippingResponse->getContent());
        
        $cartId = $request->getSession()->get('cartId');

        if($cartId == null){
            return $this->redirectToRoute('home');
        }
        
        if ($request->isMethod('POST')) {
            
            $restClient = $this->container->get('circle.restclient');

            $customerObject["formaPago"] = 7;
            $customerObject["currency"] = "CLP";
            $customerObject["method_shipping_id"] = intval($request->get('method_shipping_id'));
          
            $customerObject["costumerName"] = $request->get('costumerName');
            $customerObject["costumerPhone"] = $request->get('costumerPhone');
            $customerObject["costumerEmail"] = $request->get('costumerEmail');
            $customerObject["shippingName"] = $request->get('shippingName');
            $customerObject["shippingRut"] = $request->get('shippingRut');
            $customerObject["shippingCity"] = $request->get('shippingCity');
            $customerObject["shippingCommune"] = $request->get('shippingCommune');
            $customerObject["shippingAddress"] = $request->get('shippingAddress');
            $customerObject["billingName"] = $request->get('shippingName');
            $customerObject["billingRut"] =  $request->get('shippingRut');
            $customerObject["billingCity"] = $request->get('shippingCity');
            $customerObject["billingCommune"] =  $request->get('shippingCommune');
            $customerObject["billingAddress"] = $request->get('shippingAddress');

            $customer = json_encode($customerObject);
            $apiToken = $this->container->getParameter('api_token');
            $response = $restClient->post($apiUrl.'products/shopping-cart/checkout/'.$cartId.'?access_token='.$apiToken,$customer);
            $response = json_decode($response->getContent());
            $response = $response[0];
            
            $cartId = $response->cart_id;
            $cartCode = $response->cart_code;
            $price = $response->converted_total_price;
            $pagoBase64 = base64_encode($cartId."@;".$cartCode."@;".$price);
            $urlBase = $response->payment_url;
            //$request->getSession()->remove('cartId');
            $webpayURL = $urlBase.'?data='.$pagoBase64;
            
            return $this->redirect($webpayURL);
        }
        $response = $restClient->get($apiUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken);
    
        $cart = json_decode($response->getContent()); 
        if(count($cart) == 1){
            $cart = $cart[0];
        }
        $parameters["cart"] = $cart;
        $parameters["shippingMethods"] = $shippingMethods;
        // replace this example code with whatever you need
        return $this->render('@App/plantilla/resumen.html.twig', $parameters);
    }

    public function changeQuantityShoppingCartAction(Request $request)
    {
        $apiToken = $this->container->getParameter('api_token');
        $restClient = $this->container->get('circle.restclient');
        $apiUrl = $this->container->getParameter('api_url');
        $locale = $request->getLocale();

        $variant_Id = $request->query->get('variantId');
        $product_Id = $request->query->get('productId');
        $quantity_products = $request->query->get('quantity_products');
        $cart_Id = $request->query->get('cartId');
        $discounted_price = $request->query->get('discountedPrice');
        
        try {
            $response = $restClient->get($apiUrl.'products/shopping-cart/change/quantity?variant_Id='.$variant_Id.'&product_Id='.$product_Id.'&quantity_products='.$quantity_products.'&cart_Id='.$cart_Id.'&discounted_price='.$discounted_price.'&access_token='.$apiToken);
            
            $content_array = json_decode($response->getContent());
            
            $parameters["products"] = $content_array;
            $parameters["locale"] = $locale;
          } catch (Circle\RestClientBundle\Exceptions\OperationTimedOutException $exception) {
            
          }
        
          return new JsonResponse($response->getContent());
    }

}
