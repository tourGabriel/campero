<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $first_name = strip_tags(trim($_POST["clientName"]));
		$first_name = str_replace(array("\r","\n"),array(" "," "),$first_name);
        $email = filter_var(trim($_POST["clientEmail"]), FILTER_SANITIZE_EMAIL);
        $subject = trim($_POST["clientSubject"]);
        $message = trim($_POST["clientMessage"]);

        // Check that data was sent to the mailer.
        if ( empty($first_name) OR empty($subject) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Por favor, completa el formulario e inténtalo de nuevo.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "castilloj.catalina@gmail.com";

        // Set the email subject.
        $subject = "$subject";

        // Build the email content.
        $email_content = "Nomnre: $first_name\n";
        $email_content .= "Correo: $email\n\n";
        $email_content .= "Asunto: $subject\n\n";
        $email_content .= "Mensaje:\n$message\n";

        // Build the email headers.
        $email_headers = "De: $first_name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "¡Gracias! Tu mensaje ha sido enviado";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "¡OOPS!, Algo fue mal y no logramos enviar tu mensaje.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Hubo problemas con el envío, por favor inténtalo de nuevo ";
    }

?>
